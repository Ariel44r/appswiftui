//
//  ContentView.swift
//  appSwiftUI
//
//  Created by ARIEL DIAZ on 18/03/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var text: String = ""
    var body: some View {
        TabView {
            Text("First View")
                .tabItem {
                    Image(systemName: "1.circle")
                    Text("First")
                }.tag(0)
            Text("Second View")
                .tabItem {
                    Image(systemName: "2.circle")
                    Text("Second")
                }.tag(1)
            VStack {
                TextField("placeholder", text: self.$text)
                
                Text(self.text)
                    .padding()
                
                Button(action: {
                    debugPrint("didSelectButton")
                    debugPrint(self.text)
                }) {
                    Text("Oh my dear button!")

                }
                .padding()
                .shadow(radius: 10)
                .overlay(
                    RoundedRectangle(cornerRadius: 20)
                        .stroke(Color.blue, lineWidth: 2)
                )
            }.tabItem {
                Image(systemName: "3.circle")
                Text("Third")
            }.tag(2)
        }.accentColor(.red)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
